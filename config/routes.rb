Rails.application.routes.draw do

  devise_for :users
  get 'login/index'
  resources :posts do
    resources :comments
  end

  devise_scope :user do
    authenticated :user do
      root 'posts#index', as: :authenticated_root
    end
    unauthenticated :user do
      root to: "devise/sessions#new", as: :unauthenticate_root
    end
  end


  root 'login#index'
end
