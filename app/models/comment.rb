class Comment < ActiveRecord::Base
  belongs_to :post
  # Ensures post_id is associated with the comment
  validates :post, presence: true
  # Validates presence of Comment's body
  validates :body, presence: true
  belongs_to :user
end
