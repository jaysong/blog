class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  # Validates title is present
  validates :title, presence: true
  # Validates body is present
  validates :body, presence: true
  belongs_to :user
end
